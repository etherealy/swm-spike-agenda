# Spike Agenda: Notes - Définif une nouvelle API de READ pour les rendez-vous

## Existant

### Chargement des rdv dans le planning

- Agenda -> planning ressources 
  - planning vacations : medecins ou salles
  - planning ressource
  - planning presentation : plusieurs ressources (modèle de ressources)
     - planning patient souvent ajouté dynamiquement au planning de présentation pour replannifier un rdv par exemple

planning ressource = planning presentation avec 1 ressource

Ressources : 
- Professionnel de Santé
- Chambre
- Equipement
- Pool / Groupe
- Matériel
- Patient
- Unité Fonctionnelle

Action struts planning => PlanningPriseRDV.java 

Methode `loadRDV` est le point d'entrée, directement appelée par le front (4i)

DTO_App et DTO_Sys passés à la méthode. 
DTO_App contient des infos provenant du client (liste des ressources à charger (types, ID, ...), ...)
DTO_Sys contient les infos de la sessions. (id etab, id user, ...)

Un RDV est une activité avec différents types possible:  

	- VACATION("Vacation")  
	- ABSENCE("Absence")  
	- INDISPONIBILITE("Indisponibilite")  
	- JOURNEE_TYPE("JourneeType")  
	- RENDEZ_VOUS("RendezVous")  
	- SEANCE_GROUPE("RendezVousSeance")  
	- VACATION_PATIENT("VacationRattachementPatient")  

Cette activité est ensuite transformée en créneau
Le créneaux est ensuite enrichi de données pour l'afficher

#### "API" existantes

Identifier les appelants du service agenda

Définitions JAX-RS:

```xml
<jaxrs:server id="rdvareplanifier" address="/rdvareplanifier">
        <jaxrs:serviceBeans>
            <bean class="com.softwaymedical.ws.rs.agenda.rdv.areplanifier.webservices.RdvAReplanifierWS" />
        </jaxrs:serviceBeans>
        <jaxrs:providers>
            <bean class="org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider"/>
        </jaxrs:providers>
    </jaxrs:server>
    <jaxrs:server id="docexterne" address="/docexterne">
        <jaxrs:serviceBeans>
            <bean class="com.softwaymedical.ws.rs.agenda.docexterne.webservices.DocExterneWS" />
        </jaxrs:serviceBeans>
        <jaxrs:providers>
            <bean class="org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider"/>
        </jaxrs:providers>
    </jaxrs:server>
    <jaxrs:server id="supprimervacations" address="/supprimervacations">
        <jaxrs:serviceBeans>
            <bean class="com.softwaymedical.ws.rs.agenda.vacation.supprimer.webservices.SupprimerVacationsWs"/>
        </jaxrs:serviceBeans>
        <jaxrs:providers>
            <ref bean="mobileJsonProvider" />
            <bean class="org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider"/>
        </jaxrs:providers>
    </jaxrs:server>
    <jaxrs:server id="accueilanticipe" address="/accueilanticipe">
        <jaxrs:serviceBeans>
            <bean class="com.softwaymedical.ws.rs.agenda.accueil.anticipe.AccueilAnticipeWs"/>
        </jaxrs:serviceBeans>
        <jaxrs:providers>
            <ref bean="mobileJsonProvider" />
            <bean class="org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider"/>
        </jaxrs:providers>
    </jaxrs:server>
    <jaxrs:server id="changermultivacations" address="/changermultivacations">
        <jaxrs:serviceBeans>
            <bean class="com.softwaymedical.ws.rs.agenda.vacation.modifier.webservices.ChangerMultiVacationsWs"/>
        </jaxrs:serviceBeans>
        <jaxrs:providers>
            <ref bean="mobileJsonProvider" />
            <bean class="org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider"/>
        </jaxrs:providers>
    </jaxrs:server>
    <jaxrs:server id="planningpreference" address="/planning/preference">
        <jaxrs:serviceBeans>
            <bean class="com.softwaymedical.ws.rs.agenda.planning.preference.PlanningPreferenceWs"/>
        </jaxrs:serviceBeans>
        <jaxrs:providers>
            <ref bean="mobileJsonProvider" />
            <bean class="org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider"/>
        </jaxrs:providers>
    </jaxrs:server>
    <jaxrs:server id="planninghabilitation" address="/planning/habilitation">
        <jaxrs:serviceBeans>
            <bean class="com.softwaymedical.ws.rs.agenda.planning.drawer.webservice.PlanningHabilitationsWs"/>
        </jaxrs:serviceBeans>
        <jaxrs:providers>
            <ref bean="mobileJsonProvider" />
            <bean class="org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider"/>
        </jaxrs:providers>
    </jaxrs:server>
    <jaxrs:server id="propager" address="/propager/recurrence">
        <jaxrs:serviceBeans>
            <bean class="com.softwaymedical.ws.rs.agenda.creneau.recurrence.webservices.propager.v1.PropagerWS" />
        </jaxrs:serviceBeans>
        <jaxrs:providers>
            <bean class="org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider"/>
        </jaxrs:providers>
    </jaxrs:server>
    <jaxrs:server id="rdvmixte" address="/rdvmixte">
        <jaxrs:serviceBeans>
            <bean class="com.softwaymedical.ws.rs.agenda.rdvmixte.webservices.RdvMixteWS" />
        </jaxrs:serviceBeans>
        <jaxrs:providers>
            <bean class="org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider"/>
        </jaxrs:providers>
    </jaxrs:server>
```

```xml
<jaxrs:server id="planningPatientImpression" address="/planningpatient/impression">
        <jaxrs:serviceBeans>
            <bean class="com.softwaymedical.ws.rs.agenda.planning.patient.webservices.impression.v1.PlanningPatientImpressionWS" />
        </jaxrs:serviceBeans>
        <jaxrs:providers>
            <bean class="org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider"/>
        </jaxrs:providers>
    </jaxrs:server>
```

ITO a une API d'export de rdv qui accède directement aux données

### Question pour Sabine:

- Comment on intégère les données venant de l'extérieur (ex Docto-lib) ?

## Réalisations

Scope limité au load du planning d'un patient

### Tests de caractérisation

- Test de caractérisation de la méthode loadRDV();

  - On récupère le XML des DTO_App et DTO_Sys au Runtime pour l'utiliser dans notre test
  - ~~On a pas trouver de méthode permettant de générer un IModelDTO depuis un XML~~
    - ~~On créé un helper permettant d'instancier un DynaDTOBean qui implémente IModelDTO depuis un XML ...~~
  - On a fini par trouver une méthode DTOUtil.deserializeFromXML(String xmlData) ... 
  - la méthode loadRDV() dépent aussi de la session utilisateur il nous faut donc instancier une session utilisateur pour nos tests ...

En passant par Shiro:

```
DefaultSecurityManager securityManager = new org.apache.shiro.mgt.DefaultSecurityManager();
List<Realm> realms = new ArrayList<Realm>();
realms.add(new DefaultRealm());
securityManager.setRealms(realms);
SecurityUtils.setSecurityManager(securityManager);
UsernamePasswordModeToken token = new UsernamePasswordModeToken("jojo", "jojo", false, HMConnexionMode.DEFAULT, null);
SecurityUtils.getSubject().login(token);

//extract from SecurityAction.setUserSessionState
UserSessionState state = getSpringLocator().getBean(SmUserSessionState.class).computeUserStateAtLogin();
//getSpringLocator().getBean(LogMultiOnglet.class).login(state, idUser);
SecurityUtils.getSubject().getSession().setAttribute(UserSessionState.class.getName(), state);
```

Nous avions besoin de set l'idEtab dans le BusinessContext

On fait fail le test avec un assert pour récupérer le xml de sortie de la méthode.
Puis on assert que la sortie de la méthode soit egal au to string du xml que l'on a récupéré precedemment.
En comparant avec la sortie au Runtime, on se rend compte que les valeurs de cetains des nodes du xml sont des listes dont l'ordonnancement est non déterministe.
   
### Design de l'API
Ubiquitous Language:
Utilisateur : Personne utilisatrice de HM avec ses habilatations
Planning ou Agenda : Vue calendaire
Creneau ou Evenement : Construit à partir des activités avec un complément d'informations pour ?
Ressource : Patient, Salle, Professionnel de santé, Machine

Exemple:
base: https://www.softwayapis.com

path : /api/agenda/activites

path: /agenda/v1/agendas
path: /agenda/v1/agendas/{agendaId}
path: /agenda/v1/agendas/{agendaId}/activites
path: /agenda/v1/agendas/{agendaId}/activites/{activiteId}

path: /agenda/v1/agendas
- ressource: {ressourceId}
- debut: {dateDebut}
- fin: {dateFin}

path: /agenda/v1/ressources/{ressourceId}/activites/{activiteId} ? (On sort du domaine agenda)

### Rapport d'etonnement
un evenement par resource lié par un id_etsa 

### Suite
- Faire "reculer" l'emissaire producteur (descendre les couches): insertion deuxieme rails pour comment s'abstraire de la base de donnée
- quels sont les données dont on a imperativement besoin aujourd'hui et comment le restructurer (en ajoutant une nouvelle table)
- reflechir/implementer l'api du loadRDV pour un patient (juste lecture ou ecriture)
  - switch au niveau de la methode dans le backend
  - switch au niveau du frontend
- continuer la caracterisation (faciliter la gestion du contexte utilisateur)
- deployer un swagger-ui like sur HM
- refactor de la methode loadRdv apres caracterisation

### Interrogation
- consacrer du temps et pas atelier par ateliers

### Emissaire Producteur
### Emissaire Consommateur (où se placer ? Pas coté front dans un premier temps ?)

